/*
 * WiiChuckDemo -- 
 *
 * 2008 Tod E. Kurt, http://thingm.com/
 * - analog in 2
 * + analog in 3
 * d analog in 4
 * c analog in 5
 // rojo a 5v
// marron gnd
// naranja digital 9
 */
#include <Servo.h> 
#include <Wire.h>
#include "nunchuck_funcs.h"

// Valores que devuelve el joystick del nunchuck
int nunchuck_minx=30;
int nunchuck_maxx=224;
int nunchuck_miny=34;
int nunchuck_maxy=223;
float joyx_ratio=(nunchuck_maxx-nunchuck_minx)/100;
float joyy_ratio=(nunchuck_maxy-nunchuck_miny)/100;
// servos
Servo horizontal_servo;
Servo vertical_servo;

// pins
int horizontal_servo_pin=8;
int vertical_servo_pin=9;
// rango de rotacion de los servos
int horizontal_servo_max_rotation = 160;
int horizontal_servo_min_rotation = 10;
int vertical_servo_max_rotation = 160;
int vertical_servo_min_rotation = 10;
// estado del nunchuck
int joyx,joyy,zbut,cbut;
float xpos,ypos;
//
int speed_quot=10;
int updateDelay = 64;


void setup()
{
    Serial.begin(19200);
    nunchuck_setpowerpins();
    nunchuck_init();
    
    horizontal_servo.attach(horizontal_servo_pin);
    vertical_servo.attach(vertical_servo_pin); 
}

void reset()
{
  xpos=(horizontal_servo_max_rotation-horizontal_servo_min_rotation)/2;
  ypos=(vertical_servo_max_rotation-vertical_servo_min_rotation)/2;
  Serial.print("\nRESET!!!!!!\n\n ");
}

void loop()
{

        nunchuck_get_data();
        
        zbut = nunchuck_zbutton(); // 0..1
        cbut = nunchuck_cbutton(); // 0..1
        
        joyx = ((nunchuck_joyx()-nunchuck_minx)*(joyx_ratio))-100;    // 30..224 --> 0..100
        joyy = ((nunchuck_joyy()-nunchuck_miny)*(joyy_ratio))-100;    // 34..223 --> 0..100
        
        if(cbut==1){reset();};
        if(zbut==1){speed_quot+=5;if(speed_quot>30){speed_quot=5;}};
        
        xpos+=(joyx*-1)/speed_quot; 
        if(xpos>horizontal_servo_max_rotation){xpos=horizontal_servo_max_rotation;};
        if(xpos<horizontal_servo_min_rotation){xpos=horizontal_servo_min_rotation;};
      
        ypos+=joyy/speed_quot;
        if(ypos>vertical_servo_max_rotation){ypos=vertical_servo_max_rotation;};
        if(ypos<vertical_servo_min_rotation){ypos=vertical_servo_min_rotation;};
       
        Serial.print("\tjoyx: "); Serial.print(joyx);
        /*
        Serial.print("\tjoyx: "); Serial.print(joyx);
        Serial.print("\tjoyy: "); Serial.print(joyy);
        Serial.print("\tzbut: "); Serial.print(zbut);
        Serial.print("\tcbut: "); Serial.println(cbut);
        Serial.print("\n\tx: "); Serial.println(xpos);
        Serial.print("\ty: "); Serial.println(ypos);
        */
        horizontal_servo.write(xpos);
        vertical_servo.write(ypos);
        
        delay(updateDelay);
}

