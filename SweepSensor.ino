// rojo a 5v
// marron gnd
// naranja digital 9
#include <Servo.h> 
 
Servo USSensorServo;  
int USSensorMaxRotation = 160;
int USSensorMinRotation = 10;
int USSensorDelay = 50;
int pos = (USSensorMinRotation-USSensorMaxRotation)/2;  

boolean forward = true;

void setup(){ 
  USSensorServo.attach(9);   
  USSensorServo.write(pos);
} 
 
void loop() 
{ 
  if(pos>USSensorMaxRotation){forward=false;};
  if(pos<USSensorMinRotation){forward=true;};
  
  if(forward){
    pos++;
  }else{
    pos--;
  }
  
  USSensorServo.write(pos); 
  delay(USSensorDelay);
} 
